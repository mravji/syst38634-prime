package sheridan;

public class Prime {
	
	
	public static boolean isPrime( int number ) {
		for ( int i = 2 ; i <= number /2 ; i ++) {
			if ( number % i == 0  ) {
				return false;
			}
		}
		return true;
 	}

	public static void main(String[] args) {
		

		System.out.println( "Number 29 is prime? " + Prime.isPrime( 29 ) );
		System.out.println( "Number 29 is prime? " + Prime.isPrime( 30 ) );		
		System.out.println( "Number 33 is prime? " + Prime.isPrime( 33 ) );
		System.out.println( "Number 34 is prime? " + Prime.isPrime( 34 ) );	
		System.out.println( "Number 35 is prime? " + Prime.isPrime( 35 ) );		
		System.out.println( "Number 36 is prime? " + Prime.isPrime( 36 ) );	
		
		// Written by Mayur
		System.out.println( "Number 48 is prime? " + Prime.isPrime( 48 ) );
		//written by yours truly Kamil
		System.out.println("Number 37 is prime? "+ Prime.isPrime(37));
		//written by alex
		System.out.println("number 47 is prime?" + Prime.isPrime(47));
		


		//written by kamil

		System .out.println("Number 47 is prime"+ Prime.isPrime(47));

		// Written again by Mayur
		
		System.out.println( "Number 51 is prime? " + Prime.isPrime( 51 ) );

		//also written by alex
		System.out.println("number 74 is prime?" + Prime.isPrime(74));

	}

}
